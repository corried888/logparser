package com.digio.parser;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LogParserTest {

    String path = "src/main/resources/programming-task-example-data.log";
    String path2 = "src/main/resources/example_data2.log";
    String path3 = "src/main/resources/example_data_1_line.log";

    @Test
    void testNumberOfUniqueIPs() throws IOException {
        LogParser lp = new LogParser(path);
        assertEquals(11, lp.getNumberOfUniqueIPs());
    }

    @Test
    void testTop3VisitedURLs() throws IOException {
        LogParser lp = new LogParser(path);
        List<String> top3 = new ArrayList<>();
        top3.add("/docs/manage-websites/");
        top3.add("/blog/2018/08/survey-your-opinion-matters/");
        top3.add("/newsletter/");
        assertFalse(lp.getTop3VisitedURLs().isEmpty());
        assertEquals(top3, lp.getTop3VisitedURLs());
    }

    @Test
    void testTop3ActiveIPs() throws IOException {
        LogParser lp = new LogParser(path);
        List<String> top3 = new ArrayList<>();
        top3.add("168.41.191.40");
        top3.add("50.112.00.11");
        top3.add("177.71.128.21");
        assertFalse(lp.getTop3ActiveIpAddresses().isEmpty());
        assertEquals(top3, lp.getTop3ActiveIpAddresses());
    }

    @Test
    void testIOException() throws IOException {
        assertThrows(IOException.class, () -> {LogParser lp = new LogParser("");});
    }

    @Test
    void testNumberOfUniqueIPs2() throws IOException {
        LogParser lp = new LogParser(path2);
        assertEquals(12, lp.getNumberOfUniqueIPs());
    }

    @Test
    void testTop3VisitedURLs2() throws IOException {
        LogParser lp = new LogParser(path2);
        List<String> top3 = new ArrayList<>();
        top3.add("/intranet-analytics/");
        top3.add("/docs/manage-websites/");
        top3.add("/blog/2018/08/survey-your-opinion-matters/");
        assertFalse(lp.getTop3VisitedURLs().isEmpty());
        assertEquals(top3, lp.getTop3VisitedURLs());
    }

    @Test
    void testTop3ActiveIPs2() throws IOException {
        LogParser lp = new LogParser(path2);
        List<String> top3 = new ArrayList<>();
        top3.add("177.71.128.21");
        top3.add("168.41.191.40");
        top3.add("50.112.00.11");
        assertFalse(lp.getTop3ActiveIpAddresses().isEmpty());
        assertEquals(top3, lp.getTop3ActiveIpAddresses());
    }

    @Test
    void testNumberOfUniqueIPs1Line() throws IOException {
        LogParser lp = new LogParser(path3);
        assertEquals(1, lp.getNumberOfUniqueIPs());
    }

    @Test
    void testTop3VisitedURLs1Line() throws IOException {
        LogParser lp = new LogParser(path3);
        List<String> top3 = new ArrayList<>();
        top3.add("/intranet-analytics/");
        assertFalse(lp.getTop3VisitedURLs().isEmpty());
        assertEquals(top3, lp.getTop3VisitedURLs());
    }

    @Test
    void testTop3ActiveIPs1Line() throws IOException {
        LogParser lp = new LogParser(path3);
        List<String> top3 = new ArrayList<>();
        top3.add("177.71.128.22");
        assertFalse(lp.getTop3ActiveIpAddresses().isEmpty());
        assertEquals(top3, lp.getTop3ActiveIpAddresses());
    }

}