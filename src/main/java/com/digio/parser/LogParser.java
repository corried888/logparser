package com.digio.parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;

public class LogParser {

    Logger logger = Logger.getLogger(LogParser.class);

    private String filePath = "";
    private Integer numberOfUniqueIPs;
    private List<String> top3VisitedURLs;
    private List<String> top3ActiveIPs;

    public LogParser(String filePath) throws IOException {
        this.filePath = filePath;
        this.numberOfUniqueIPs = 0;
        this.top3VisitedURLs = new ArrayList<>();
        this.top3ActiveIPs = new ArrayList<>();
        this.init();
    }

    public void init() throws IOException {
        List<String> allURLs = new ArrayList<>();
        List<String> allIPs = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileInputStream(filePath))) {
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                // Add all IPs to the list
                allIPs.add(line.substring(0, line.indexOf("-")-1));
                // Add all URLs to the List
                allURLs.add(extractURLFromString(line));
            }
            // number of unique IPs
            List<String> listDistinctIPs = allIPs.stream().distinct().collect(Collectors.toList());
            numberOfUniqueIPs = listDistinctIPs.size();
            // top 3 visited URLs
            this.top3VisitedURLs = setTop3OfList(allURLs);
            // top 3 active IPs
            this.top3ActiveIPs = setTop3OfList(allIPs);
        }
        catch (IOException ie){
            logger.error("Error reading file: " + ie.getMessage());
            throw ie;
        }
    }

    private String extractURLFromString(String line){
        int i1 = line.indexOf('"');
        int i2 = (line.substring(i1)).indexOf(" ") + 1;
        int i3 = (line.substring(i1 + i2)).indexOf(" ");

        return line.substring(i1+i2,i1+i2+i3);
    }

    private List<String> setTop3OfList(List<String> list) {
        List<String> top3 = new ArrayList<>();
        Map<String, Long> top3URLMap = list.stream()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        top3URLMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(3)
                .collect(Collectors.toList())
                .forEach(entry -> top3.add(entry.getKey()));
        return top3;
    }

    public Integer getNumberOfUniqueIPs() {
        return this.numberOfUniqueIPs;
    }

    public List<String> getTop3VisitedURLs() {
        return this.top3VisitedURLs;
    }

    public List<String> getTop3ActiveIpAddresses() {
        return this.top3ActiveIPs;
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 1){
            try {
                LogParser lp = new LogParser(args[0]);
                System.out.println("The number of unique IP addresses: " + lp.getNumberOfUniqueIPs());
                System.out.println("The top 3 most visited URLs: " + lp.getTop3VisitedURLs());
                System.out.println("The top 3 most active IP addresses: " + lp.getTop3ActiveIpAddresses());
            }
            catch (IOException ie) {
                System.err.println("Something went wrong. Please make sure the path is correct.");
            }
        } else {
            System.out.println("Please specify the file path as the only argument.");
        }

    }

}
