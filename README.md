# README #

### What is this repository for? ###
#### Summary
This is an example log parser. Given a specific log file containing HTTP requests: 
* Calculate the number of unique IP addresses
* Filter to get the top 3 most visited URLs
* Filter to get the most active IP addresses

### Design considerations
* Log file can become very large, make sure the file is read efficiently.
* Currently, the program expects a "fixed structure" log file.  

### How do I get set up? ###

* Check out the repo.
* Dependencies are managed in Maven.
* Once built you can run using -> java -jar logparser-1.0.jar "path/to/log/file"